﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Text.Json;
using WebApplication10.Models;

namespace WebApplication10.Controllers
{
    public class AppController : Controller
    {

        public IActionResult Register()
        {
            return View();
        }


        public IActionResult Small()
        {
            return View();
        }

        public IActionResult Error()
        {
            return View();
        }

        public IActionResult ProductForms()
        {
            int? numProd = TempData["ProducForms"] as int?;
            ViewData["NumProd"] = numProd;
            return View();
        }

        public IActionResult ProductsNumber()
        {
            return View();
        }

        public IActionResult Products()
        {
            string? productsSerealized = TempData["ProductsInfo"] as string;
            if (productsSerealized == null)
            {
                return BadRequest("Order data not found.");
            }
            List<ProductViewModel> products = JsonSerializer.Deserialize<List<ProductViewModel>>(productsSerealized);
            ViewData["Products"] = products;
            return View();
        }

        [HttpPost]
        public IActionResult Register(UserViewModel user)
        {
            if (user.Age < 16)
            {
                return RedirectToAction(nameof(Small));
            }
            return RedirectToAction(nameof(ProductsNumber));
        }


        [HttpPost]
        public IActionResult ProductsNumber(int numProd)
        {
            if (numProd <= 0)
            {
                return RedirectToAction(nameof(Error));
            }
            TempData["ProducForms"] = numProd;
            return RedirectToAction(nameof(ProductForms));
        }

        [HttpPost]
        public IActionResult FormResult(List<ProductViewModel> products)
        {
            foreach (ProductViewModel product in products)
            {
                Console.WriteLine("///");
                Console.WriteLine(product.Name);
            }
            string serealized = JsonSerializer.Serialize(products);
            TempData["ProductsInfo"] = serealized;
            return RedirectToAction(nameof(Products));
        }
    }
}
