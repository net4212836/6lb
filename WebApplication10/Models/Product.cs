﻿using System.Runtime.Serialization;

namespace WebApplication10.Models
{
    public class ProductViewModel
    {
        public string Size { get; set; } = "";
        public string Name { get; set; } = "";
        public int Count { get; set; }

        public ProductViewModel() { }
    }
}
