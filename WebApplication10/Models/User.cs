﻿namespace WebApplication10.Models
{
    public class UserViewModel
    {

        public string Password { get; set; } = "";
        public string Email { get; set; } = "";
        public int Age { get; set; }
    }
}
